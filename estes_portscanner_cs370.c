#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>

int main(int argc, char *argv[]) {
  int err, i, j, k, sock;
  long start, end;
  char *token;
  char *tofree;

  char hostname_input[100];
  char hostname_list[100][100];
  char hostname[100];
  int hostname_count;

  const char s[2] = ",";
  const char z[2] = "-";

  char port_input[100];
  char port_list[100][100];
  char port[100];
  int port_count;

  struct hostent *host;
  struct sockaddr_in sa;

  //Deal with user input
  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-p") == 0) {
      port_count = 0;
      token = strtok(argv[i + 1], s);
      while (token != NULL) {
        strcpy(port_list[port_count], token);
        token = strtok(NULL, s);
        port_count++;
      }
    }

    if (strcmp(argv[i], "-h") == 0) {
      hostname_count = 0;
      token = strtok(argv[i + 1], s);
      while (token != NULL) {
        strcpy(hostname_list[hostname_count], token);
        token = strtok(NULL, s);
        hostname_count++;
      }
    }
  }

  // Initialise
  strncpy((char *)&sa, "", sizeof sa);
  sa.sin_family = AF_INET;

  // Main Loop
  for (j = 0; j < hostname_count; j++) {
     // setup ip
    strcpy(hostname, hostname_list[j]);
    // ip address
    printf("%s :\n",hostname);
    if (isdigit(hostname[0])) {
      sa.sin_addr.s_addr = inet_addr(hostname);
      printf("ip accepted\n");
    }
    // hostname ip address
    else if ((host = gethostbyname(hostname)) != 0) {
      strncpy((char *)&sa.sin_addr, (char *)host->h_addr, sizeof sa.sin_addr);
      printf("hostname accepted\n");
    } else

      herror(hostname);
  }

  // Start the port scan loop
  for (k = 0; k < port_count; k++) {

    if (strchr(port_list[k], '-') != NULL) {
      token = strtok(port_list[k], z);
      start = atoi(token);
      token = strtok(NULL, z);
      end = atoi(token);
    } else {
      start = atoi(port_list[k]);
      end = atoi(port_list[k]);
    }

    printf("\n\nScanning ports %ld - %ld:\n", start, end);
    for (i = start; i <= end; i++) {

      sa.sin_port = htons(i);

      sock = socket(AF_INET, SOCK_STREAM, 0);
      if (sock < 0) {
        perror("\nSocket");
        exit(1);
      }

      err = connect(sock, (struct sockaddr *)&sa, sizeof sa);

      // not open
      if (err < 0) {
        // printf("%s %-5d %s\r" , hostname , i, strerror(errno));
        fflush(stdout);
      }
      // open
      else {
        printf("%-5d open\n", i);
      }
      close(sock);
    }
  }
  return (0);
}
